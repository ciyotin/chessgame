//BOARD_H
#ifndef BOARD_H
#define BOARD_H
#include "square.h"



class Board
{
	Square square[8][8];
	Color turn = WHITE;
	string playedMoves;
	int moveKing(Square* thisKing, Square* thatSpace);
	int moveQueen(Square* thisQueen, Square* thatSpace);
	int moveBishop(Square* thisBishop, Square* thatSpace);
	int moveKnight(Square* thisKnight, Square* thatSpace);
	int moveRook(Square* thisRook, Square* thatSpace);
	int movePawn(Square* thisPawn, Square* thatSpace);
	int makeMove(int x1, int y1, int x2, int y2);
	void printBoard();
	int checkMate();

public:
	void clrPlayedMoves() { playedMoves = ""; }
	Board() { playedMoves = ""; }
	Square* getSquare(int x, int y) {

		try
		{
			return &square[x][y];
		}
		catch (exception e)
		{
			e.what();
			cout << endl << e.what() << endl;
			getchar();
		}

	}
	
	void setSquare(Square * s, int x, int y) {
		square[x][y] = *s;
	}
	bool doMove(int);

	void setBoard();
	bool playGame(int);

	string getNextMove(string position, int diff);
	void CloseConnection();
	string calcNextMove(string position, string path2, int diff);
};





#endif