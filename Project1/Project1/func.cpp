﻿#include "func.h"
#include "board.h"
#include "square.h"
#include <string>
#include <iostream>
#include <stdio.h>
#include <sstream>
#include <speechapi_cxx.h>

using namespace std;

using namespace Microsoft::CognitiveServices::Speech;

string ltoNumber(string move)
{

	if (move[0] == 'a')move[0] = '0';
	else if (move[0] == 'b')move[0] = '1';
	else if (move[0] == 'c')move[0] = '2';
	else if (move[0] == 'd')move[0] = '3';
	else if (move[0] == 'e')move[0] = '4';
	else if (move[0] == 'f')move[0] = '5';
	else if (move[0] == 'g')move[0] = '6';
	else if (move[0] == 'h')move[0] = '7';

	if (move[2] == 'a')move[2] = '0';
	else if (move[2] == 'b')move[2] = '1';
	else if (move[2] == 'c')move[2] = '2';
	else if (move[2] == 'd')move[2] = '3';
	else if (move[2] == 'e')move[2] = '4';
	else if (move[2] == 'f')move[2] = '5';
	else if (move[2] == 'g')move[2] = '6';
	else if (move[2] == 'h')move[2] = '7';

	if (move[1] == '1')move[1] = '0';
	else if (move[1] == '2')move[1] = '1';
	else if (move[1] == '3')move[1] = '2';
	else if (move[1] == '4')move[1] = '3';
	else if (move[1] == '5')move[1] = '4';
	else if (move[1] == '6')move[1] = '5';
	else if (move[1] == '7')move[1] = '6';
	else if (move[1] == '8')move[1] = '7';

	if (move[3] == '1')move[3] = '0';
	else if (move[3] == '2')move[3] = '1';
	else if (move[3] == '3')move[3] = '2';
	else if (move[3] == '4')move[3] = '3';
	else if (move[3] == '5')move[3] = '4';
	else if (move[3] == '6')move[3] = '5';
	else if (move[3] == '7')move[3] = '6';
	else if (move[3] == '8')move[3] = '7';

	return move;
}

string voiceMove() {
	string move = "";
	string token1;
	string token2;
	string token3;
	string token4;
	string lastToken;

	bool flag = false;
	while (!flag) {

		move = recognizeSpeech();
		transform(move.begin(), move.end(), move.begin(), ::tolower);

		while (true) {
		istringstream stream(move);
		stream >> token1 >> token2 >> token3 >> token4;
		lastToken = token4.substr(0, token4.size() - 1);



		if (token2 == "one") token2 = "1";

		else if (token2 == "tree") token2 = "3";

		else if (token2 == "to") token2 = "2";

		else if (token2 == "fourth" || token2 == "fort") token2 = "4";

		if (lastToken == "one") lastToken = "1";

		else if (lastToken == "tree") lastToken = "3";

		else if (lastToken == "to") lastToken = "2";

		else if (lastToken == "fourth" || lastToken == "fort") lastToken = "4";

		

		if (token1 == "he") token1 = "e";

		if (token3 == "he") token3 = "e";

		if (token1 == "hey," || token1 == "hey") token1 = "a";

		if (token3 == "hey," || token3 == "hey") token3 = "a";

		if (token3 == "age") token3 = "h";

		if (token1 == "age") token1 = "h";
		


		if (!(token1[0] >= 'a' && token1[0] <= 'h') || !(token3[0] >= 'a' && token3[0] <= 'h')) {
			
			cout << "Vermis oldugun harfleri kontrol et." << endl;
			
			cout << "\nPress enter to make move...\n" << endl;
			getchar();

			move = recognizeSpeech();
			transform(move.begin(), move.end(), move.begin(), ::tolower);
		}
		else if ( !(token2[0] >= '0' && token2[0] <= '9') || !(lastToken[0] >= '0' && lastToken[0] <= '9') ) {
			
			cout << "Vermis oldugun sayilari kontrol et." << endl;
			
			cout << "\nPress enter to make move...\n" << endl;
			getchar();
			
			move = recognizeSpeech();
			transform(move.begin(), move.end(), move.begin(), ::tolower);
		}
		else
		{
			
			move = token1 + token2 + token3 + lastToken;
			cout << "Did you mean -> " << move << endl;
			cout << "Evet icin - > 1" << endl;
			cout << "Hayir icin - > 0 "<< endl;
			cin >> flag;
			break;
		}
	
		}


	}

	move = token1 + token2 + token3 + lastToken;
	return move;
}

string recognizeSpeech()
{
	// Creates an instance of a speech config with specified subscription key and service region.
	// Replace with your own subscription key and service region (e.g., "westus").
	auto config = SpeechConfig::FromSubscription("101935706519428a82fc302b08c58ada", "westus");

	// Creates a speech recognizer.
	auto recognizer = SpeechRecognizer::FromConfig(config);
	cout << "I'm listening...\n";

	// Performs recognition. RecognizeOnceAsync() returns when the first utterance has been recognized,
	// so it is suitable only for single shot recognition like command or query. For long-running
	// recognition, use StartContinuousRecognitionAsync() instead.
	auto result = recognizer->RecognizeOnceAsync().get();

	// Checks result.
	if (result->Reason == ResultReason::RecognizedSpeech)
	{
		/*cout << "We recognized: " << result->Text << std::endl;*/
	}
	else if (result->Reason == ResultReason::NoMatch)
	{
		cout << "NOMATCH: Speech could not be recognized." << std::endl;
	}
	else if (result->Reason == ResultReason::Canceled)
	{
		auto cancellation = CancellationDetails::FromResult(result);
		cout << "CANCELED: Reason=" << (int)cancellation->Reason << std::endl;

		if (cancellation->Reason == CancellationReason::Error)
		{
			cout << "CANCELED: ErrorCode= " << (int)cancellation->ErrorCode << std::endl;
			cout << "CANCELED: ErrorDetails=" << cancellation->ErrorDetails << std::endl;
			cout << "CANCELED: Did you update the subscription info?" << std::endl;
		}
	}
	return result->Text;
}
