﻿#include "board.h"
#include "square.h"
#include "func.h"
#include <windows.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <sstream>
#include <speechapi_cxx.h>


using namespace std;

using namespace Microsoft::CognitiveServices::Speech;

string recognizeSpeech();
string voiceMove();

STARTUPINFO sti = { 0 };
SECURITY_ATTRIBUTES sats = { 0 };
PROCESS_INFORMATION pi = { 0 };
HANDLE pipin_w, pipin_r, pipout_w, pipout_r;
BYTE buffer[10240];
DWORD writ, excode, read, available;




void Board::printBoard() {
	if (turn == BLACK) {
		for (int i = 0; i <= 7; i++)
		{
			cout << " " << i + 1 << "   ";
			for (int j = 7; j >= 0; j--)
			{

				Piece p = square[j][i].getPiece();
				Color c = square[j][i].getColor();

				switch (p)
				{
				case KING: (c == WHITE) ? cout << " K " : cout << " k ";
					break;
				case QUEEN: (c == WHITE) ? cout << " Q " : cout << " q ";
					break;
				case BISHOP:(c == WHITE) ? cout << " B " : cout << " b ";
					break;
				case KNIGHT:(c == WHITE) ? cout << " H " : cout << " h ";
					break;
				case ROOK: (c == WHITE) ? cout << " R " : cout << " r ";
					break;
				case PAWN: (c == WHITE) ? cout << " P " : cout << " p ";
					break;
				case EMPTY: cout << " " << "*" << " ";
					break;
				default: cout << "XXX";
					break;
				}

			}

			cout << endl;
		}
		cout << "\n      h  g  f  e  d  c  b  a \n" << endl;
	
	}
	else {

		for (int i = 7; i >= 0; i--)
		{
			cout << " " << i + 1 << "   ";
			for (int j = 0; j < 8; j++)
			{

				Piece p = square[j][i].getPiece();
				Color c = square[j][i].getColor();

				switch (p)
				{
				case KING: (c == WHITE) ? cout << " K " : cout << " k ";
					break;
				case QUEEN: (c == WHITE) ? cout << " Q " : cout << " q ";
					break;
				case BISHOP:(c == WHITE) ? cout << " B " : cout << " b ";
					break;
				case KNIGHT:(c == WHITE) ? cout << " H " : cout << " h ";
					break;
				case ROOK: (c == WHITE) ? cout << " R " : cout << " r ";
					break;
				case PAWN: (c == WHITE) ? cout << " P " : cout << " p ";
					break;
				case EMPTY: cout << " " << "*" << " ";
					break;
				default: cout << "XXX";
					break;
				}

			}

			cout << endl;
		}
	
	cout << "\n      a  b  c  d  e  f  g  h \n" << endl;
}
	
	if (playedMoves != "")
	{
		char* lastMove = const_cast<char*>(playedMoves.c_str());
		int a = playedMoves.size();
		cout << "\n\nLast played move was = ";
		for (int i = a - 6; i < a; i++)
			cout << lastMove[i];
		cout << endl;
	}
}


bool Board::doMove(int gameType) {
	string move = "", move2 = "";
	int x1, x2, y1, y2;
	bool stop = false;

	while (!stop)
	{
		if (move == "(non" || move == "0000")
		{
			system("cls");
			printBoard();
			if (turn == 1)
				cout << "WHITE\t";
			else
				cout << "BLACK\t";
			cout << "WINS  !!!!!!!!!!!!!!!\n\n";
			return false;
		}

		if (checkMate() == 0)
		{
			return false;
		}

		if (gameType == 1)
		{
			if (turn == WHITE)
			{
				cout << "White make your move" << endl;
				cin >> move;
			}
			else
			{
				cout << "Black make your move" << endl;
				cin >> move;
			}
		}
		else
		{

			if (turn == WHITE)
			{
				if (gameType == 3) {
					cout << "Press enter to make move...\n" << endl;
					 getchar();
					getchar();
					
					move = voiceMove();
				}
				else if (gameType == 4)
				{
					cout << "Make your move" << endl;
					cin >> move;
				}
				else if (gameType == 5)
				{
					move = calcNextMove(playedMoves, "stockfish_10_x64.exe", 2000);
				}
				else if (gameType == 2) {
					cout << "White make your move " << endl;
					getchar();
					getchar();
					move = voiceMove();
				}

			}
			
		else if (gameType == 2) {
				cout << "Black make your move " << endl;
				getchar();
				getchar();
				move = voiceMove();
				
		}
		else
			{
				move = calcNextMove(playedMoves, "stockfish_10_x64.exe", 2000);

			}
		}

		move2 = ltoNumber(move);

		x1 = move2[0] - 48;
		y1 = move2[1] - 48;
		x2 = move2[2] - 48;
		y2 = move2[3] - 48;
		if (getSquare(x1, y1)->getColor() == turn)
		{

			int temp;
			if ((temp = makeMove(x1, y1, x2, y2)) == 0)
			{
				cout << "Invalid move, try again." << endl;
			}
			else if (temp == 1) {
				playedMoves = playedMoves + move + "q ";
				
				stop = true;
			}
			else if (temp == 2) {
				playedMoves = playedMoves + move + "b ";
				
				stop = true;
			}
			else if (temp == 3) {
				playedMoves = playedMoves + move + "n ";
				
				stop = true;
			}
			else if (temp == 4) {
				playedMoves = playedMoves + move + "r ";
				
				stop = true;
			}
			else
			{
				playedMoves = playedMoves + move + " ";
				stop = true;
			}

		}
		else
			cout << "That's not your piece. Try again." << endl;

	}


	if (turn == BLACK)
		turn = WHITE;
	else
		turn = BLACK;

	return true;

}

void Board::setBoard()
{
	square[0][0].setPieceAndColor(ROOK, WHITE);
	square[1][0].setPieceAndColor(KNIGHT, WHITE);
	square[2][0].setPieceAndColor(BISHOP, WHITE);
	square[3][0].setPieceAndColor(QUEEN, WHITE);
	square[4][0].setPieceAndColor(KING, WHITE);
	square[5][0].setPieceAndColor(BISHOP, WHITE);
	square[6][0].setPieceAndColor(KNIGHT, WHITE);
	square[7][0].setPieceAndColor(ROOK, WHITE);

	square[0][7].setPieceAndColor(ROOK, BLACK);
	square[1][7].setPieceAndColor(KNIGHT, BLACK);
	square[2][7].setPieceAndColor(BISHOP, BLACK);
	square[3][7].setPieceAndColor(QUEEN, BLACK);
	square[4][7].setPieceAndColor(KING, BLACK);
	square[5][7].setPieceAndColor(BISHOP, BLACK);
	square[6][7].setPieceAndColor(KNIGHT, BLACK);
	square[7][7].setPieceAndColor(ROOK, BLACK);

	for (int i = 0; i < 8; i++)
	{
		square[i][1].setPieceAndColor(PAWN, WHITE);
		square[i][6].setPieceAndColor(PAWN, BLACK);

	}
	for (int i = 2; i < 6; i++)
	{
		for (int j = 0; j < 8; j++)
			square[j][i].setPieceAndColor(EMPTY, NONE);

	}
	for (int i = 0; i < 8; i++)
		for (int j = 0; j < 8; j++)
		{
			square[i][j].setX(i);
			square[i][j].setY(j);
		}

}

int Board::checkMate()
{
	bool w = 0, b = 0;
	int i, j;
	for (int i = 0; i < 8; i++)
	{
		for (j = 0; j < 8; j++)
		{
			if (square[i][j].getPiece() == KING && square[i][j].getColor() == WHITE)
				w = 1;
			if (square[i][j].getPiece() == KING && square[i][j].getColor() == BLACK)
				b = 1;
		}
	}
	if (w == 0)
	{
		system("cls");
		printBoard();
		cout << "\nBLACK WINS  !!!!!!!!!!!!!!!";
		return 0;
	}
	else if (b == 0)
	{
		system("cls");
		printBoard();
		cout << "\nWHITE WINS  !!!!!!!!!!!!!!!";
		return 0;
	}
	return 1;
}

bool Board::playGame(int gameType)
{
	system("cls");
	printBoard();
	return doMove(gameType);
}

int Board::moveKing(Square* thisKing, Square* thatSpace)
{

	if (thisKing->getX() == 4 && thisKing->getY() == 0 && thisKing->getColor() == WHITE)
	{
		if (thatSpace->getX() == 6 && thatSpace->getY() == 0 && getSquare(5, 0)->getColor() == NONE &&
			getSquare(6, 0)->getColor() == NONE && getSquare(7, 0)->getColor() == WHITE && getSquare(7, 0)->getPiece() == ROOK)
		{
			//kisa rok
			moveRook(getSquare(7, 0), getSquare(5, 0));
			thatSpace->setSpace(thisKing);
			thisKing->setEmpty();
			return 61; 

		}
		else if (thatSpace->getX() == 2 && thatSpace->getY() == 0 && getSquare(1, 0)->getColor() == NONE &&
			getSquare(2, 0)->getColor() == NONE && getSquare(3, 0)->getColor() == NONE && getSquare(0, 0)->getColor() == WHITE && getSquare(0, 0)->getPiece() == ROOK)
		{
			moveRook(getSquare(0, 0), getSquare(3, 0));
			thatSpace->setSpace(thisKing);
			thisKing->setEmpty();
			return 61;
		}
		
	}

	else if (thisKing->getX() == 4 && thisKing->getY() == 7 && thisKing->getColor() == BLACK)
	{
		if (thatSpace->getX() == 6 && thatSpace->getY() == 7 && getSquare(5, 7)->getColor() == NONE &&
			getSquare(6, 7)->getColor() == NONE && getSquare(7, 7)->getColor() == BLACK && getSquare(7, 7)->getPiece() == ROOK)
		{
			moveRook(getSquare(7, 7), getSquare(5, 7));
			thatSpace->setSpace(thisKing);
			thisKing->setEmpty();
			return 61;
		}
		else if (thatSpace->getX() == 2 && thatSpace->getY() == 7 && getSquare(1, 7)->getColor() == NONE &&
			getSquare(2, 7)->getColor() == NONE && getSquare(3, 7)->getColor() == NONE && getSquare(0, 7)->getColor() == BLACK && getSquare(0, 7)->getPiece() == ROOK)
		{
			moveRook(getSquare(0, 7), getSquare(3, 7));
			thatSpace->setSpace(thisKing);
			thisKing->setEmpty();
			return 61;
			//uzun rok
			
		}
	}
	if ((abs(thisKing->getX() - thatSpace->getX()) <= 1 && abs(thisKing->getY() - thatSpace->getY()) <= 1))
	{
		thatSpace->setSpace(thisKing);
		thisKing->setEmpty();
		return 61;

	}

	return false;
}


int Board::moveQueen(Square* thisQueen, Square* thatSpace) {

	
	int queenX = thisQueen->getX();
	int queenY = thisQueen->getY();
	int thatX = thatSpace->getX();
	int thatY = thatSpace->getY();
	int yIncrement;
	int xIncrement;

	bool invalid = false;
	if (queenX != thatX || queenY != thatY)
	{

		if (queenX == thatX)
		{
			yIncrement = (thatY - queenY) / (abs(thatY - queenY));
			for (int i = queenY + yIncrement; i != thatY; i += yIncrement)
			{

				if (square[thatX][i].getColor() != NONE)
					return false;

			}
		}
		else
			if (queenY == thatY)
			{

				xIncrement = (thatX - queenX) / (abs(thatX - queenX));
				for (int i = queenX + xIncrement; i != thatX; i += xIncrement)
				{
					if (square[i][thatY].getColor() != NONE)
						return false;
				}
			}
			else
				if (abs(queenX - thatX) == abs(queenY - thatY))
				{
					xIncrement = (thatX - queenX) / (abs(thatX - queenX));
					yIncrement = (thatY - queenY) / (abs(thatY - queenY));

					for (int i = 1; i < abs(queenX - thatX); i++)
					{
						if (square[queenX + xIncrement * i][queenY + yIncrement * i].getColor() != NONE)
							return false;

					}
				}
				else
					return false;
		
	}



	if (invalid == false)
	{
		thatSpace->setSpace(thisQueen);
		thisQueen->setEmpty();
		return 61;
	}
	else
	{
		return false;
	}
}

int Board::moveBishop(Square* thisBishop, Square* thatSpace) { 
	int bishopX = thisBishop->getX();
	int bishopY = thisBishop->getY();
	int thatX = thatSpace->getX();
	int thatY = thatSpace->getY();
	bool invalid = false;
	Square *s;
	if (abs(bishopX - thatX) == abs(bishopY - thatY))
	{
		int xIncrement = (thatX - bishopX) / (abs(thatX - bishopX));
		int yIncrement = (thatY - bishopY) / (abs(thatY - bishopY));

		for (int i = 1; i < abs(bishopX - thatX); i++)
		{
			if (square[bishopX + xIncrement * i][bishopY + yIncrement * i].getColor() != NONE)
				return false;

		}
	}
	else
		return false;

	if (invalid == false)
	{
		thatSpace->setSpace(thisBishop);
		thisBishop->setEmpty();
		return 61;
	}
	else
	{
		return false;
	}
}

int Board::moveKnight(Square* thisKnight, Square* thatSpace)
{
	
	int knightX = thisKnight->getX();
	int knightY = thisKnight->getY();
	int thatX = thatSpace->getX();
	int thatY = thatSpace->getY();

	if ((abs(knightX - thatX) == 2 && abs(knightY - thatY) == 1) || (abs(knightX - thatX) == 1 && abs(knightY - thatY) == 2))
	{
		thatSpace->setSpace(thisKnight);
		thisKnight->setEmpty();
		return 61;
	}
	else
	{
		return false;
	}
}

int Board::moveRook(Square* thisRook, Square* thatSpace)
{
	
	int rookX = thisRook->getX();
	int rookY = thisRook->getY();
	int thatX = thatSpace->getX();
	int thatY = thatSpace->getY();
	bool invalid = false;
	if (rookX != thatX || rookY != thatY)
	{

		if (rookX == thatX)
		{
			int yIncrement = (thatY - rookY) / (abs(thatY - rookY));
			for (int i = rookY + yIncrement; i != thatY; i += yIncrement)
			{

				if (square[thatX][i].getColor() != NONE)
					return false;

			}
		}
		else
			if (rookY == thatY)
			{

				int xIncrement = (thatX - rookX) / (abs(thatX - rookX));
				for (int i = rookX + xIncrement; i != thatX; i += xIncrement)
				{
					if (square[i][thatY].getColor() != NONE)
						return false;
				}
			}
			else
				return false;
	}
	if (invalid == false)
	{
		thatSpace->setSpace(thisRook);
		thisRook->setEmpty();
		return 61;
	}
	else
	{
		std::cout << "That is an invalid move for rook";
		return false;
	}
}

int Board::movePawn(Square* thisPawn, Square* thatSpace) {
	
	bool invalid = false;
	int pawnX = thisPawn->getX();
	int pawnY = thisPawn->getY();
	int thatX = thatSpace->getX();
	int thatY = thatSpace->getY();


	if (thisPawn->getColor() == WHITE)
	{

		if (((pawnX == thatX && thatY == pawnY + 1) && thatSpace->getColor() == NONE))
		{
			thatSpace->setSpace(thisPawn);
			thisPawn->setEmpty();

			if (thatSpace->getY() == 7)
			{
				int a;
				cout << "For \n\tQUEEN -> 1\n\tBISHOP -> 2\n\tKNIGHT -> 3\n\tROOK-> 4\n";
				cin >> a;
				Piece newPiece = static_cast<Piece>(a);
				thatSpace->setPieceAndColor(newPiece, WHITE);

				return a;
			}

			return 61;
		}
		else if ((pawnX + 1 == thatX || pawnX - 1 == thatX) && pawnY + 1 == thatY && thatSpace->getColor() == BLACK)
		{
			thatSpace->setSpace(thisPawn);
			thisPawn->setEmpty();

			if (thatSpace->getY() == 7)
			{
				int a;
				cout << "For \n\tQUEEN -> 1\n\tBISHOP -> 2\n\tKNIGHT -> 3\n\tROOK-> 4\n";
				cin >> a;
				Piece newPiece = static_cast<Piece>(a);
				thatSpace->setPieceAndColor(newPiece, WHITE);
				return a;
			}

			return 61;
		}
		else if (pawnY == 1 && ((pawnX == thatX && thatY == pawnY + 2) && thatSpace->getColor() == NONE && getSquare(thatX, thatY - 1)->getColor() == NONE))
		{
			thatSpace->setSpace(thisPawn);
			thisPawn->setEmpty();
			return 61;
		}

		else
			return false;
	}
	else
	{
		if (thisPawn->getColor() == BLACK)
		{
			if (pawnX == thatX && thatY == pawnY - 1 && thatSpace->getColor() == NONE)
			{
				thatSpace->setSpace(thisPawn);
				thisPawn->setEmpty();

				if (thatSpace->getY() == 0)
				{
					int a;
					cout << "For \n\tQUEEN -> 1\n\tBISHOP -> 2\n\tKNIGHT -> 3\n\tROOK-> 4\n";
					cin >> a;
					Piece newPiece = static_cast<Piece>(a);
					thatSpace->setPieceAndColor(newPiece, BLACK);
					return a;
				}

				return 61;
			}
			else
				if ((pawnX + 1 == thatX || pawnX - 1 == thatX) && pawnY - 1 == thatY && thatSpace->getColor() == WHITE)
				{
					thatSpace->setSpace(thisPawn);
					thisPawn->setEmpty();

					if (thatSpace->getY() == 0)
					{
						int a;
						cout << "For \n\tQUEEN -> 1\n\tBISHOP -> 2\n\tKNIGHT -> 3\n\tROOK-> 4\n";
						cin >> a;
						Piece newPiece = static_cast<Piece>(a);
						thatSpace->setPieceAndColor(newPiece, BLACK);
						return a;
					}

					return 61;
				}
				else if (pawnY == 6 && ((pawnX == thatX && thatY == pawnY - 2) && thatSpace->getColor() == NONE && getSquare(thatX, thatY + 1)->getColor() == NONE))
				{
					thatSpace->setSpace(thisPawn);
					thisPawn->setEmpty();
					return 61;
				}
				else
					return false;
		}
		else
			return false;
	}
}

int Board::makeMove(int x1, int y1, int x2, int y2) {
	
	if (x1 < 0 || x1>7 || y1 < 0 || y1>7 || x2 < 0 || x2>7 || y2 < 0 || y2>8)
	{
		std::cout << "One of your inputs was out of bounds" << std::endl;
		return false;
	}
	Square* src = getSquare(x1, y1);
	Square* dest = getSquare(x2, y2);

	if (src->getColor() == dest->getColor() && dest->getColor() != NONE)
	{
		std::cout << "Invalid move: cannot land on your own piece" << std::endl;
		return false;
	}

	switch (src->getPiece())
	{
	case KING: return moveKing(src, dest);
		break;
	case QUEEN: return moveQueen(src, dest);
		break;
	case BISHOP: return moveBishop(src, dest);
		break;
	case KNIGHT: return moveKnight(src, dest);
		break;
	case ROOK: return moveRook(src, dest);
		break;
	case PAWN: return movePawn(src, dest);
		break;
	case EMPTY: std::cout << "You do not have a piece there" << std::endl;  return false;
		break;
	default: std::cerr << "Something went wrong in the switch statement in makeMove()" << std::endl;
		break;
	}
	return false;
}

string Board::calcNextMove(string position, string path2, int diff)
{
	pipin_w = pipin_r = pipout_w = pipout_r = NULL;
	sats.nLength = sizeof(sats);
	sats.bInheritHandle = TRUE;
	sats.lpSecurityDescriptor = NULL;

	CreatePipe(&pipout_r, &pipout_w, &sats, 0);
	CreatePipe(&pipin_r, &pipin_w, &sats, 0);

	sti.dwFlags = STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES;
	sti.wShowWindow = SW_HIDE;
	sti.hStdInput = pipin_r;
	sti.hStdOutput = pipout_w;
	sti.hStdError = pipout_w;

	
	LPSTR path = const_cast<char *>(path2.c_str());

	CreateProcess(NULL, path, NULL, NULL, TRUE, 0, NULL, NULL, &sti, &pi);

	string nextMove = getNextMove(position, diff);

	CloseConnection();

	return nextMove;
}

string Board::getNextMove(string position, int diff)
{
	string str;
	string difftostr = to_string(diff);
	string position2 = "position startpos moves " + position + "\ngo " + difftostr + "\n";
	int temp = position.size();
	WriteFile(pipin_w, position2.c_str(), position2.length(), &writ, NULL);
	Sleep(2 * diff); // diff -> AI'in düsünme zamani

	PeekNamedPipe(pipout_r, buffer, sizeof(buffer), &read, &available, NULL);
	do
	{
		ZeroMemory(buffer, sizeof(buffer));
		if (!ReadFile(pipout_r, buffer, sizeof(buffer), &read, NULL) || !read) break;
		buffer[read] = 0;
		str += (char*)buffer;
	} while (read >= sizeof(buffer));

	

	int n = str.find("bestmove");
	if (n != -1) return str.substr(n + 9, 4);

	int m = str.size();
	for (int i = 0; i < m - 70; i++)
	{
		str[i] = '*';
	}
	n = str.find(" pv ");
	cout << endl << str.substr(n + 4, 4) << endl;
	return str.substr(n + 4, 4);

	
}

void Board::CloseConnection()
{
	WriteFile(pipin_w, "quit\n", 5, &writ, NULL);
	if (pipin_w != NULL) CloseHandle(pipin_w);
	if (pipin_r != NULL) CloseHandle(pipin_r);
	if (pipout_w != NULL) CloseHandle(pipout_w);
	if (pipout_r != NULL) CloseHandle(pipout_r);
	if (pi.hProcess != NULL) CloseHandle(pi.hProcess);
	if (pi.hThread != NULL) CloseHandle(pi.hThread);
}
